﻿using System.Collections.Generic;
using System.Linq;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public static class DisplayFeed
    {
        public static class Columns
        {
            public const int Region = 0;
            public const int DevName = 1;
            public const int DevId = 2;
            public const int Price = 3;
            public const int PriceFrom = 4;
            public const int DevUrl = 5;
            public const int ImgUrl = 6;
            public const int Address = 7;
            public const int TownName = 8;
        }

        public static List<Row> AsRows(this IList<IList<object>> data)
        {
            var rows = new List<Row>();
            if (data == null || !data.Any())
                return rows;

            // starts with 1 because row #0 is the titles
            for (var i = 1; i < data.Count; i++)
            {
                var row = data[i];

                rows.Add(new Row
                {
                    RowNumber = i,
                    DevName = row[Columns.DevName].ToString(),
                    Price = row[Columns.Price].ToString(),
                    DevUrl = row[Columns.DevUrl].ToString(),
                    ImgUrl = row[Columns.ImgUrl].ToString(),
                    TownName = row[Columns.TownName].ToString()
                });
            }

            return rows;
        }
    }
}
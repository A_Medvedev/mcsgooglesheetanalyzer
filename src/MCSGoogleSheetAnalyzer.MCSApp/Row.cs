﻿namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class Row
    {
        public int RowNumber { get; set; }
        public string DevName { get; set; }
        public string Price { get; set; }
        public string DevUrl { get; set; }
        public string ImgUrl { get; set; }
        public string TownName { get; set; }
    }
}
﻿using MCSGoogleSheetAnalyzer.MCSApp.Extensions;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class Result
    {
        public int RowNumber { get; }
        public string Url { get; }
        public CheckingResultType Type { get; }
        
        public string Message => Type.GetStringValue();

        public Result(int rowNumber, string url, CheckingResultType type)
        {
            RowNumber = rowNumber;
            Url = url;
            Type = type;
        }

        public override string ToString()
        {
            return $"{RowNumber} | {Url} | {Type} | {Type.GetStringValue()}";
        }
    }
}
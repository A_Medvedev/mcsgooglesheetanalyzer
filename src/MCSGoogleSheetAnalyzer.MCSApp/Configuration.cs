﻿using System.Configuration;
using System.Linq;
using MCSGoogleSheetAnalyzer.MCSApp.Extensions;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class AppSettingsConfigurationManager : IConfigurationManager
    {
        public string GetSetting(string settingName, string defaultValue = null)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
                return defaultValue;

            var settingValue = ConfigurationManager.AppSettings[settingName];

            return settingValue.IsNotEmpty()
                ? settingValue
                : defaultValue;
        }

        public int GetSetting(string settingName, int defaultValue)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
                return defaultValue;

            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (settingValue.IsNotEmpty() && int.TryParse(settingValue, out var parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        public string[] GetCommaSeparatedSetting(string settingName)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
                return new string[0];

            var settingValue = ConfigurationManager.AppSettings[settingName];

            return settingValue.IsNotEmpty()
                ? settingValue.Split(',')
                : new string[0];
        }
    }
}
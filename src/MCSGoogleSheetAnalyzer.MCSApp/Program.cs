﻿using System;
using System.Collections.Generic;
using System.Linq;
using MCSGoogleSheetAnalyzer.MCSApp.Checkers;
using MCSGoogleSheetAnalyzer.MCSApp.Extensions;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    internal static class Program
    {
        private static IConfigurationManager _appSettingsConfigManager;
        private static IWebClient _simpleWebClient;

        private static void Init()
        {
            _appSettingsConfigManager = new AppSettingsConfigurationManager();
            _simpleWebClient = new SimpleWebClient();
        }

        private static void Main()
        {
            var startProcessing = DateTime.Now;

            Init();

            var secretFilePath = _appSettingsConfigManager.GetSetting("secretFilePath");
            var applicationName = _appSettingsConfigManager.GetSetting("applicationName");
            var spreadSheetId = _appSettingsConfigManager.GetSetting("spreadSheetId");
            var range = _appSettingsConfigManager.GetSetting("range");

            if (secretFilePath.IsEmpty() || applicationName.IsEmpty() || spreadSheetId.IsEmpty() || range.IsEmpty())
                return;

            var googleService = new GoogleSheetService(secretFilePath, applicationName, spreadSheetId);
            var rows = googleService.ReadData(range).AsRows();

            var results = new List<Result>();

            results.AddRange(new DevelopmentNameLength(_appSettingsConfigManager.GetSetting("MaxDevelopmentNameLength", 0)).Check(rows));
            results.AddRange(new TownNameLength(_appSettingsConfigManager.GetSetting("MaxTownNameLength", 0)).Check(rows));
            results.AddRange(new PageIsAvailableCheck(_simpleWebClient).Check(rows));
            results.AddRange(new ImageCheck(_simpleWebClient,_appSettingsConfigManager.GetSetting("expectedImageWidth", 0), _appSettingsConfigManager.GetSetting("expectedImageHight", 0)).Check(rows));
            results.AddRange(new ChekPrice(_appSettingsConfigManager, _simpleWebClient, googleService).Check(rows));
            //results.AddRange(new ChekPrice(_appSettingsConfigManager, _simpleWebClient, googleService).CheckAndUpdate(rows, spreadSheetId));
            
            var endProcessing = DateTime.Now;

            try
            {
                Console.WriteLine("Email sending...");

                var fromEmail = _appSettingsConfigManager.GetSetting("FromEmail");
                var toEmail = _appSettingsConfigManager.GetSetting("ToEmail");
                var reportTitle = _appSettingsConfigManager.GetSetting("ReportTitle");

                if (fromEmail.IsEmpty() || toEmail.IsEmpty() || reportTitle.IsEmpty())
                    return;

                var emailSender = new EmailSender("report.html");
                emailSender.Send(fromEmail, new
                    {
                        googleSpreadsheetsUrl = _appSettingsConfigManager.GetSetting("GoogleSpreadsheetsUrl"),

                        urlCheckResults = results.Where(x => x.Type == CheckingResultType.PageIsUnavailable),
                        priceCheckResult = results.Where(x =>
                            x.Type == CheckingResultType.ThePriceIsDifferent ||
                            x.Type == CheckingResultType.PriceCheckingPageNotFound),
                        imageResults = results.Where(x => x.Type == CheckingResultType.WrongImageSize),
                        devNameCheckResults =
                        results.Where(x => x.Type == CheckingResultType.IncorrectDevelpmentNameLength),
                        townNameCheckResults = results.Where(x => x.Type == CheckingResultType.IncorrectTownNameLength),

                        time = endProcessing - startProcessing,
                    },
                    reportTitle, toEmail);
            }
            catch (Exception e)
            {
                Console.WriteLine("Email sending error");
                Console.WriteLine(e);
                throw;
            }

            Console.WriteLine("\nFinish!");
            Console.ReadKey();
        }
    }
}
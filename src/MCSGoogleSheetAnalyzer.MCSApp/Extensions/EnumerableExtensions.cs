﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MCSGoogleSheetAnalyzer.MCSApp.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            var itemList = source.ToList();

            foreach (var item in itemList)
            {
                action(item);
            }

            return itemList;
        }
    }
}
﻿using HtmlAgilityPack;
using ImageMagick;

namespace MCSGoogleSheetAnalyzer.MCSApp.Interfaces
{
    public interface IWebClient
    {
        bool IsPageAvailable(string url, bool allowAutoRedirect = false);

        MagickImageInfo GetImageInfo(string url);

        HtmlDocument GetPage(string url);
    }
}
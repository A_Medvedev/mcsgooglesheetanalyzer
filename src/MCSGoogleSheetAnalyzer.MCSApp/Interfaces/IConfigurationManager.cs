﻿namespace MCSGoogleSheetAnalyzer.MCSApp.Interfaces
{
    public interface IConfigurationManager
    {
        string GetSetting(string settingName, string defaultValue = null);
        int GetSetting(string settingName, int defaultValue);
        string[] GetCommaSeparatedSetting(string settingName);
    }
}
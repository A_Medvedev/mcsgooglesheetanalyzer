﻿namespace MCSGoogleSheetAnalyzer.MCSApp.Interfaces
{
    public interface IEmailSender
    {
        void Send(string from, object data, string subject, string to);
    }
}
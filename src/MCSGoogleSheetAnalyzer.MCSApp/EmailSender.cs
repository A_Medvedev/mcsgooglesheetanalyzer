﻿using System.IO;
using System.Net.Mail;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;
using Nustache.Core;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class EmailSender : IEmailSender
    {
        private readonly string _templateName;

        public EmailSender(string templateName)
        {
            _templateName = templateName;
        }

        public void Send(string from, object data, string subject, string to)
        {
            var template = LoadTemplate();
            var message = PrepareMessage(from, template, data, subject, to);
            using (var smtp = new SmtpClient())
            {
                smtp.Send(message);
            }
        }

        private string LoadTemplate()
        {
            using (var streamReader = new StreamReader(Path.Combine("EmailTemplates", _templateName)))
            {
                return streamReader.ReadToEnd();
            }
        }

        private static MailMessage PrepareMessage(string from, string template, object data, string subject, string to)
        {
            var message = new MailMessage
            {
                Body = Render.StringToString(template, data).Trim(),
                Subject = Render.StringToString(subject, data).Trim(),
                IsBodyHtml = true
            };
            if (!string.IsNullOrWhiteSpace(from))
                message.From = new MailAddress(from);

            message.To.Add(to);

            return message;
        }
    }
}
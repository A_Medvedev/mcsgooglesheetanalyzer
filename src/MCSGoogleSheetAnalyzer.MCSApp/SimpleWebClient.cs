﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using HtmlAgilityPack;
using ImageMagick;
using MCSGoogleSheetAnalyzer.MCSApp.Extensions;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class SimpleWebClient : IWebClient
    {
        private static HttpWebRequest CreateHttpRequeset(string url, string method = WebRequestMethods.Http.Get,
            bool allowAutoRedirect = false)
        {
            ServicePointManager.DefaultConnectionLimit = 6;
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.MaxServicePointIdleTime = 500;

            var request = WebRequest.CreateHttp(url);
            request.Timeout = 30000;
            request.Method = method;
            request.AllowAutoRedirect = allowAutoRedirect;

            return request;
        }

        public bool IsPageAvailable(string url, bool allowAutoRedirect = false)
        {
            if (url.IsEmpty())
                return false;

            try
            {
                using (var response =
                    (HttpWebResponse) CreateHttpRequeset(url, WebRequestMethods.Http.Head).GetResponse())
                {
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch
            {
                return false;
            }
        }

        public MagickImageInfo GetImageInfo(string url)
        {
            if (url.IsEmpty())
                return null;

            try
            {
                using (var response = (HttpWebResponse) CreateHttpRequeset(url).GetResponse())
                {
                    var responseStream = response.GetResponseStream();
                    return new MagickImageInfo(responseStream);
                }
            }
            catch (Exception e)
            {
                // todo: log
                return null;
            }
        }

        public HtmlDocument GetPage(string url)
        {
            if (url.IsEmpty())
                return null;

            try
            {
                using (var response = (HttpWebResponse) CreateHttpRequeset(url).GetResponse())
                {
                    var stream = response.GetResponseStream();
                    using (var reader = new StreamReader(stream))
                    {
                        var html = reader.ReadToEnd();
                        var doc = new HtmlDocument();
                        doc.LoadHtml(html);
                        return doc;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
﻿using MCSGoogleSheetAnalyzer.MCSApp.Attributes;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public enum CheckingResultType
    {
        [StringValue("incorrect development length")]
        IncorrectDevelpmentNameLength,

        [StringValue("incorrect town length")]
        IncorrectTownNameLength,

        [StringValue("uage is unavailable")]
        PageIsUnavailable,

        [StringValue("url is empty")]
        UrlIsEmpty,

        [StringValue("missed protocol(http or https)")]
        MissedProtocol,

        [StringValue("wrong image size")]
        WrongImageSize,

        [StringValue("Price checking. Page not found")]
        PriceCheckingPageNotFound,

        [StringValue("the price is different")]
        ThePriceIsDifferent
    }
}
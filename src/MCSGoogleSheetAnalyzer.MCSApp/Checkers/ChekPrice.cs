﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Apis.Sheets.v4.Data;
using HtmlAgilityPack;
using MCSGoogleSheetAnalyzer.MCSApp.Extensions;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public class ChekPrice : BaseChecker
    {
        private readonly IWebClient _simpleClient;
        private readonly GoogleSheetService _googleSheetService;
        private readonly IConfigurationManager _configurationManager;

        public ChekPrice(IConfigurationManager configurationManager, IWebClient simpleClient,
            GoogleSheetService googleSheetService)
        {
            _simpleClient = simpleClient;
            _googleSheetService = googleSheetService;
            _configurationManager = configurationManager;
        }

        public IEnumerable<Result> CheckAndUpdate(IReadOnlyList<Row> rows, string destinationSheet)
        {
            var unavailablePages = rows.Where(x => !_simpleClient.IsPageAvailable(x.DevUrl)).ToList();
            var notFoundPages = unavailablePages
                .Select(x => new Result(x.RowNumber, x.DevUrl, CheckingResultType.PriceCheckingPageNotFound));

            var results = new List<Result>();
            var request = new List<Request>();

            foreach (var row in rows.Except(unavailablePages))
            {
                var price = GetPriceIfDifferent(row.DevUrl, row.Price);
                if (price.IsNotEmpty())
                {
                    results.Add(new Result(row.RowNumber, row.DevUrl, CheckingResultType.ThePriceIsDifferent));

                    if (double.TryParse(price, out var thePrice))
                    {
                        request.Add(CreateUpdateCellsRequest(_googleSheetService.GetSheetId(destinationSheet), row.RowNumber,
                            DisplayFeed.Columns.Price, thePrice));
                    }
                }
            }

            if (request.Any())
                _googleSheetService.ExecuteBatchSpreadsheetRequest(request);

            results.AddRange(notFoundPages);
            return results.OrderBy(x => x.RowNumber);
        }

        public override IEnumerable<Result> Check(IEnumerable<Row> rows)
        {
            return rows
                .AsParallel()
                .AsOrdered()
                .Where(x => !IsValidPrice(x.DevUrl, x.Price))
                .Select(x => new Result(x.RowNumber, x.DevUrl, CheckingResultType.ThePriceIsDifferent));
        }

        public override Result Check(Row row)
        {
            return !IsValidPrice(row.DevUrl, row.Price)
                ? new Result(row.RowNumber, row.DevUrl, CheckingResultType.ThePriceIsDifferent)
                : null;
        }

        private bool IsValidPrice(string url, string expectedPrice)
        {
            return GetPriceIfDifferent(url, expectedPrice).IsEmpty();
        }

        private string GetPriceIfDifferent(string url, string expectedPrice)
        {
            var htmlDoc = _simpleClient.GetPage(url);
            if (htmlDoc == null)
                return string.Empty;

            var actualPrice = FindPrice(htmlDoc);
            var currencySymbol = _configurationManager.GetSetting("MiQCurrencySymbol").FirstOrDefault();
            expectedPrice = expectedPrice.Trim(currencySymbol);

            return expectedPrice.Equals(actualPrice, StringComparison.OrdinalIgnoreCase)
                ? string.Empty
                : actualPrice;
        }

        private string FindPrice(HtmlDocument htmlDocument)
        {
            var priceXPath = _configurationManager.GetSetting("MiQPriceXPath");
            return htmlDocument.DocumentNode.SelectSingleNode(priceXPath)?.InnerText ?? string.Empty;
        }
    }
}
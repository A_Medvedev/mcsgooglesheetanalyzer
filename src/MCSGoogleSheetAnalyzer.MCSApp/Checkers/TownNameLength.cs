﻿using System.Collections.Generic;
using System.Linq;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public class TownNameLength : BaseChecker
    {
        private readonly int _maxLength;

        public TownNameLength(int maxLength)
        {
            _maxLength = maxLength;
        }

        public override IEnumerable<Result> Check(IEnumerable<Row> rows)
        {
            return rows
                .AsParallel()
                .AsOrdered()
                .Where(x => !IsNotEmptyAndCheckLength(x.TownName, _maxLength))
                .Select(x => new Result(x.RowNumber, x.TownName, CheckingResultType.IncorrectTownNameLength));
        }

        public override Result Check(Row row)
        {
            return !IsNotEmptyAndCheckLength(row.TownName, _maxLength)
                ? new Result(row.RowNumber, row.TownName, CheckingResultType.IncorrectTownNameLength)
                : null;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public class PageIsAvailableCheck : BaseChecker
    {
        private static IWebClient _simpleWebClient;
        private static bool _allowRedirects;

        public PageIsAvailableCheck(IWebClient simpleWebClient, bool allowRedirects = false)
        {
            _simpleWebClient = simpleWebClient;
            _allowRedirects = allowRedirects;
        }

        public override IEnumerable<Result> Check(IEnumerable<Row> rows)
        {
            // todo: empty urls checking
            // todo: urls without schema

            return rows
                .AsParallel()
                .AsOrdered()
                .Where(x => !_simpleWebClient.IsPageAvailable(x.DevUrl, _allowRedirects))
                .Select(x => new Result(x.RowNumber, x.DevUrl, CheckingResultType.PageIsUnavailable));
        }

        public override Result Check(Row row)
        {
            return !_simpleWebClient.IsPageAvailable(row.DevUrl, _allowRedirects)
                ? new Result(row.RowNumber, row.DevUrl, CheckingResultType.PageIsUnavailable)
                : null;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Google.Apis.Sheets.v4.Data;
using MCSGoogleSheetAnalyzer.MCSApp.Extensions;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public abstract class BaseChecker
    {
        public abstract IEnumerable<Result> Check(IEnumerable<Row> rows);

        public abstract Result Check(Row row);

        //public abstract IEnumerable<Result> CheckAndUpdate(IReadOnlyList<Row> rows, string destinationSheet);

        protected static bool IsNotEmptyAndCheckLength(string value, int maxLengh)
        {
            return value.IsNotEmpty() && value.Trim().Length <= maxLengh;
        }

        protected static Request CreateUpdateCellsRequest(int? sheetId, int rowNumber, int columnNumber, double value)
        {
            return new Request
            {
                UpdateCells = new UpdateCellsRequest
                {
                    Start = new GridCoordinate
                    {
                        SheetId = sheetId,
                        RowIndex = rowNumber,
                        ColumnIndex = columnNumber
                    },
                    Rows = new List<RowData>
                    {
                        new RowData
                        {
                            Values = new[]
                                {
                                    value
                                }
                                .Select(x => new CellData
                                {
                                    UserEnteredValue = new ExtendedValue {NumberValue = x},
                                    EffectiveFormat = new CellFormat
                                    {
                                        NumberFormat = new NumberFormat
                                        {
                                            Pattern = "",
                                            Type = "CURRENCY"
                                        }
                                    }
                                }).ToList()
                        }
                    },
                    Fields = "userEnteredValue"
                }
            };
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public class DevelopmentNameLength : BaseChecker
    {
        private readonly int _maxLength;

        public DevelopmentNameLength(int maxLength)
        {
            _maxLength = maxLength;
        }

        public override IEnumerable<Result> Check(IEnumerable<Row> rows)
        {
            return rows
                .AsParallel()
                .AsOrdered()
                .Where(x => !IsNotEmptyAndCheckLength(x.DevName, _maxLength))
                .Select(x => new Result(x.RowNumber, x.DevName, CheckingResultType.IncorrectDevelpmentNameLength));
        }

        public override Result Check(Row row)
        {
            return !IsNotEmptyAndCheckLength(row.DevName, _maxLength)
                ? new Result(row.RowNumber, row.DevName, CheckingResultType.IncorrectDevelpmentNameLength)
                : null;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using MCSGoogleSheetAnalyzer.MCSApp.Interfaces;

namespace MCSGoogleSheetAnalyzer.MCSApp.Checkers
{
    public class ImageCheck : BaseChecker
    {
        private static IWebClient _simpleWebClient;

        private readonly int _width;
        private readonly int _height;

        public ImageCheck(IWebClient simpleWebClient, int width, int height)
        {
            _simpleWebClient = simpleWebClient;

            _width = width;
            _height = height;
        }

        public override IEnumerable<Result> Check(IEnumerable<Row> rows)
        {
            return rows
                .AsParallel()
                .AsOrdered()
                .Where(x => !ImageSizeChecking(x.ImgUrl))
                .Select(x => new Result(x.RowNumber, x.ImgUrl, CheckingResultType.WrongImageSize));
        }

        public override Result Check(Row row)
        {
            throw new System.NotImplementedException();
        }

        private bool ImageSizeChecking(string url)
        {
            var img = _simpleWebClient.GetImageInfo(url);
            return img != null && img.Width == _width && img.Height == _height;
        }
    }
}
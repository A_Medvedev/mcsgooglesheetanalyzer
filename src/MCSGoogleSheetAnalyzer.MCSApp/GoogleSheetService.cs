﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace MCSGoogleSheetAnalyzer.MCSApp
{
    public class GoogleSheetService
    {
        private readonly Spreadsheet spreadSheet;
        private readonly SheetsService sheetsService;

        private readonly string spreadSheetId;

        public GoogleSheetService(string secretFilePath, string applicationName, string spreadSheetId)
        {
            this.spreadSheetId = spreadSheetId;

            var credentials = Authorize(secretFilePath);
            sheetsService = GetSheetsService(credentials, applicationName);
            spreadSheet = sheetsService.Spreadsheets.Get(spreadSheetId).Execute();
        }

        private static ServiceAccountCredential Authorize(string secretFilePath)
        {
            using (var stream = new FileStream(secretFilePath, FileMode.Open, FileAccess.Read))
            {
                return GoogleCredential.FromStream(stream)
                    .CreateScoped(SheetsService.Scope.Spreadsheets)
                    .UnderlyingCredential as ServiceAccountCredential;
            }
        }

        private static SheetsService GetSheetsService(ICredential credential, string applicationName)
        {
            return new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName
            });
        }

        public int? GetSheetId(string sheetName)
        {
            return spreadSheet.Sheets.FirstOrDefault(x => x.Properties.Title.Equals(sheetName))?.Properties.SheetId;
        }

        public void ExecuteBatchSpreadsheetRequest(IList<Request> requests)
        {
            sheetsService.Spreadsheets.BatchUpdate(new BatchUpdateSpreadsheetRequest {Requests = requests},
                spreadSheetId).Execute();
        }

        public IList<IList<object>> ReadData(string range)
        {
            return sheetsService.Spreadsheets.Values.Get(spreadSheetId, range).Execute().Values;
        }
    }
}